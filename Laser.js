/* ****Documentation****
*Laser game
*the goal is to aim the laser and hit the light sensor set up on a different arduino board.
*you use the joystick to aim, joystick button to turn laser on and off and the pot sensor to control the joysticks sensetivity.
*if the laser is on and motionless for 3 seconds it will turn off and go ro a random posiition
*
****************Connections*********************
***joystick*****
*5v to voltage
*GND to ground
*vrx to pin A0
*vry to pin A1
*SW to pin 2 // button

*****potsensor*****
*right to voltage
*left to ground
*central to pin A2

*****laser****
*right to ground
*central to voltage 
*left to pin 12

*****sevros****
**X**
*purple to ground
*red to voltage
*orange to pin 3
**Y**
*purple to ground
*red to voltage
*orange to pin 4
*/

var five = require("johnny-five");
var wait = require("wait-for-stuff");
var board = new five.Board({port:"COM3"});
var servoPossiton = {"X" : 90, "Y" : 90};  // innitial possition

const {Board, Servo, Pot} = require("johnny-five");

board.on("ready", function(){

    //set up variables and constances
    const servoX = new Servo(3);
    const servoY = new Servo(4);
    const laser = new five.Pin("12");

    var laserOn = false;  // is laser on?
    var moved = false; //have the servos moved
    var checkingIfMoved = false; //if in procces of checking moved
    var sensetivity = 4; // sensetvity of joystick
    
    //create hardware

    var potSensor = new five.Sensor("A2");
    
    //create joystick button
    joystickButton = new five.Button({
        pin: 2,
        isPullup: true
      });

  // Create a new joystick
  var joystick = new five.Joystick({
    //   [ x, y ]
    pins: ["A0", "A1"],
    inverty : true
  });


  //main program
  joystick.on("data", getAndSetXAndY);
  joystickButton.on("up", laserOnOff);//laser on/off
  potSensor.on("change",changeSensetivity);

  //functions
  function getAndSetXAndY(){
    // Get X
    if (Math.abs(this.x) > 0.1){ //prevent false readings
      moved = true;
      servoPossiton["X"]+=this.x/sensetivity;
    }
    
    if (servoPossiton["X"] > 180){
      servoPossiton["X"] = 180;
    }
    else if(servoPossiton["X"] < 0){
      servoPossiton["X"] = 0;
    }
    
    // Get Y
    if (Math.abs(this.y) > 0.1){
      moved = true;
      servoPossiton["Y"]+=this.y/sensetivity;
    }
    
    if (servoPossiton["Y"] > 180){
      servoPossiton["Y"] = 180;
    }
    else if(servoPossiton["Y"] < 0){
      servoPossiton["Y"] = 0;
    }
    // Set Servo
    servoX.to(servoPossiton["X"]);
    servoY.to(servoPossiton["Y"]);

    forceMove();
}

function forceMove(){//if laser is on and hasent moved in 1.5 seconds turn laser off and change possition
  if (!checkingIfMoved && laserOn){ 
    checkingIfMoved = true;
    board.wait(1500, function(){
      if (!moved && laserOn){
        if (laserOn){
          laserOnOff();
        }
        servoPossiton["X"] = Math.ceil(Math.random()*181 - 1); 
        servoPossiton["Y"] = Math.ceil(Math.random()*181 - 1);
      };
      moved = false;
      checkingIfMoved = false;
    });
 }
}

function laserOnOff(){//turn laser on off
    laserOn = !laserOn;
    laser.write(laserOn);
}
function changeSensetivity(){
    sensetivity = potSensor.scaleTo([1, 80])/10;
}
});
