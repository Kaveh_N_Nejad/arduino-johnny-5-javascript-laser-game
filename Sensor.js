/****Documentation
 * to accomany code from Laser_Final.js
 * this code is to control the arduino that checks wether the laser has hit its target
 * there is one light sensor
 * One green light to display when it is hitable
 * And 4 red lights to display score
 * 
 * ******conections
 * light sensor A0 and ground
 * green light 2 and ground
 * red lights 3,4,5,6 and each to ground
*/

var five = require("johnny-five");
var board = new five.Board({port:"COM6"});
board.on("ready", function() { 
    var lightSensor = new five.Light("A0");
    var sum = 0;
    var count = 0;
    var avg;
    var coolDown = false;
    var score = 0;
    var startLight = new five.Led(2);
    const scoreLights = [new five.Led(3),new five.Led(4),new five.Led(5),new five.Led(6)];

    lightSensor.on("data", function() {
        // collect room light data
        if (count === 0){
        console.log("Collecting Data");
        }
        if (count++ <= 100){
            sum += this.level;
        }  
        if (count === 100){
                avg = sum / 100;
                console.log("The game can start");
                startLight.on();
        }
        

        //Main game
        if (this.level < avg - 0.1 && !coolDown){
                coolDown = true;
                startLight.off();
                scoreLights[++score - 1].on();
                board.wait(3000, function(){//wait 3 seconds before hittable 
                    coolDown = false;
                    startLight.on();
                    
                });

            }
            if (score === 4){
                console.log("you won");
                //restart game
                for (let i in scoreLights){//turn off all red lights
                    scoreLights[i].off();
                }
                score = 0;
                count = 0;
                sum = 0;
                console.log("Start again");
            }  
    });
    
})